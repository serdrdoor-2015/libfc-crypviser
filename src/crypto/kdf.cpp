#include <fc/crypto/kdf.hpp>
#include <fc/crypto/openssl.hpp>
#include <fc/exception/exception.hpp>
#include <openssl/evp.h>
  
namespace fc {

    aes_key_secret derive_aes_key(const fc::string& password, uint64_t salt)
    {
        constexpr auto pbkdf2_iteration_count = 100000u;

        aes_key_secret secret;
        auto ret = PKCS5_PBKDF2_HMAC(
                            password.c_str(), password.length(),
                            (const unsigned char *) &salt, sizeof(salt),
                            pbkdf2_iteration_count, EVP_sha512(),
                            sizeof(aes_key_secret), (unsigned char *)  &secret);

        if (0 == ret) 
        {
            FC_THROW_EXCEPTION( aes_exception, "error during password based aes key derivation",
                            ("s", ERR_error_string( ERR_get_error(), nullptr) ) );
        }

        return secret;
    }

}
