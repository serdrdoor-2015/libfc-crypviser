#pragma once
#include <fc/string.hpp>
#include <fc/crypto/sha512.hpp>


namespace fc {
    /**
     * sha512 is not real hash. We are using it as data container
     * that includes: 256-bit AES key, 128-bit IV and 128-bit ignored
     * aes_key_secret used as input parameter (key) in 
     * aes_encrypt(const fc::sha512& key... and aes_decrypt(const fc::sha512& key...
     * see aes.hpp
     */
    using aes_key_secret = fc::sha512;

    /**
     *  @return key generated from password {256-bit AES key, 128-bit IV}
     */    
    aes_key_secret derive_aes_key(const fc::string& password, uint64_t salt = 0);
} 
